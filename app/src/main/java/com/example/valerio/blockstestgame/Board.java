package com.example.valerio.blockstestgame;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Board {
    int color[] = new int[64];
    boolean isChecked[] = new boolean[64];
    /*
    * red, blue, green, yellow, violet , orange.
    * */
    private Button button_cells[];
    private Context context;
    private int randomColorId;
    private String TAG = "Board Class";
    public Board(Button cells[], Context context){

        this.button_cells = cells;
        this.context = context;
        //this.board

        for(int i = 0; i < 64; i++) {

            randomColorId = ThreadLocalRandom.current().nextInt(1, 6 + 1);

            switch (randomColorId) {

                case 1: {
                    button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.red));
                    color[i] = ContextCompat.getColor(context, R.color.red);
                    break;
                }
                case 2: {
                    button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.blue));
                    color[i] = ContextCompat.getColor(context, R.color.blue);
                    break;
                }
                case 3: {
                    button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.green));
                    color[i] = ContextCompat.getColor(context, R.color.green);
                    break;
                }
                case 4: {
                    button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.yellow));
                    color[i] = ContextCompat.getColor(context, R.color.yellow);
                    break;
                }
                case 5: {
                    button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.violet));
                    color[i] = ContextCompat.getColor(context, R.color.violet);
                    break;
                }
                case 6: {
                    button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.orange));
                    color[i] = ContextCompat.getColor(context, R.color.orange);
                    break;
                }
            }
        }


    }
    /*
        Method that recursively find consecutive blocks
        return number of block
     */
    public int blocksFinder(int i, int j, int num_blocks){

        isChecked[i*8+j] = true;
        ArrayList <Cell> neighbor = checkNeighboar(i, j);

        if(neighbor.size() == 0)
            return 0;
        else {
            num_blocks = neighbor.size();
            for (int x = 0; x < neighbor.size(); x++) {
                num_blocks += blocksFinder(neighbor.get(x).get_i(), neighbor.get(x).get_j(), num_blocks);
            }
        }

        return num_blocks;
    }
    /*
    * Given two coordinate i and j check for all admissible neighbor
    * return arraylist of Cell
    * */
    public ArrayList<Cell> checkNeighboar(int i, int j){

        Log.d(TAG, "checkNeighboar");

        ArrayList <Cell>neighbor = new ArrayList<>();

        //sud
        if(((i+1)) < 8 && color[i*8+j] == color[(i+1)*8+j] && !isChecked[(i+1)*8+j] ){
            System.out.println("sud");
            neighbor.add(new Cell((i+1), j));
        }
        //nord
        if(((i-1)) >= 0 && color[i*8+j] == color[(i-1)*8+j] && !isChecked[(i-1)*8+j] ){
            System.out.println("nord");
            neighbor.add(new Cell((i-1), j));
        }
        //est
        if(((j+1)) < 8 && color[i*8+j] == color[(i*8)+(j+1)] && !isChecked[(i*8)+(j+1)]){
            System.out.println("est");
            neighbor.add(new Cell((i), j+1));
        }
        //ovest
        if(((j-1)) >= 0 && color[i*8+j] == color[(i*8)+(j-1)] && !isChecked[(i*8)+(j-1)]){
            System.out.println("ovest");
            neighbor.add(new Cell((i), j-1));
        }
        //System.out.println("num_blocks: "+num_blocks+" neighbor:"+neighbor.size());
        return neighbor;
    }
    /*
      check if there are possible move
    */
    public boolean isPlayable(){
        Log.d(TAG, "isPlayable");

        for (int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                ArrayList <Cell> neighbor = checkNeighboar((i), j);
                //System.out.println("neighbor isPlayable: "+neighbor.size());
                if(neighbor.size() > 0)
                    return true;
            }
        }
        return false;
    }
    /*
        reset the previous move
     */
    public void resetBoardCheck(){
        for(int i = 0; i < 64; i++){
            if(isChecked[i]){
                isChecked[i] = false;
            }
        }
    }
    /*
        change the color of pushed cells
     */
    public void updateColors(View v/*int color[], Button button_cells[], Context context*/){

        Log.d(TAG, "updateColor");
        Context context = v.getContext();
        //j*i+i

        for(int i = 0; i < 64; i++) {

            if (isChecked[i]) {

                //reset previous path
                isChecked[i] = false;
                randomColorId = ThreadLocalRandom.current().nextInt(1, 6 + 1);

                switch (randomColorId) {

                    case 1: {
                        button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.red));
                        color[i] = ContextCompat.getColor(context, R.color.red);
                        break;
                    }
                    case 2: {
                        button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.blue));
                        color[i] = ContextCompat.getColor(context, R.color.blue);
                        break;
                    }
                    case 3: {
                        button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.green));
                        color[i] = ContextCompat.getColor(context, R.color.green);
                        break;
                    }
                    case 4: {
                        button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.yellow));
                        color[i] = ContextCompat.getColor(context, R.color.yellow);
                        break;
                    }
                    case 5: {
                        button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.violet));
                        color[i] = ContextCompat.getColor(context, R.color.violet);
                        break;
                    }
                    case 6: {
                        button_cells[i].setBackgroundColor(context.getResources().getColor(R.color.orange));
                        color[i] = ContextCompat.getColor(context, R.color.orange);
                        break;
                    }
                }
            }
        }
    }
}
