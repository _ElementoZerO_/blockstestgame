package com.example.valerio.blockstestgame;

public class Cell {
    public int i;
    public int j;

    public Cell(int i, int j){
        this.i = i;
        this.j = j;
    }
    public int get_i(){
        return i;
    }
    public int get_j(){ return j; }
}
