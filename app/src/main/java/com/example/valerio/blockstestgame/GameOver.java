package com.example.valerio.blockstestgame;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GameOver extends AppCompatActivity {

    Button restart;
    TextView tv_score;
    Context context;
    String score = "YourScore:"+"\n";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gameover);
        context = this;
        restart = findViewById(R.id.btRestart);
        tv_score = findViewById(R.id.tv_score);

        Bundle extras = getIntent().getExtras();
        score += extras.getString("score");

        tv_score.setText(score);

        System.out.println(score);

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                restart();
            }
        });
    }
    public void restart(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
