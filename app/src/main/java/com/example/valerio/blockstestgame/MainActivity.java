package com.example.valerio.blockstestgame;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    Button cells[] = new Button[64];
    String buttonID;
    Board board;
    int Score, time;//120;
    TextView tvScore;
    TextView tvTime;
    Timer taskTimer = new Timer();
    Handler timerHandler;
    Handler gameoverHandler;
    MediaPlayer backSound;
    MediaPlayer matchSound;
    MediaPlayer wrongSound;
    MediaPlayer gameoverSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        System.out.println("onCreate");
        super.onCreate(savedInstanceState);
        Score = 0;
        time = 120;
        setContentView(R.layout.activity_main);

        backSound = MediaPlayer.create(MainActivity.this,R.raw.blocks_maintheme);
        backSound.setVolume(0.3f,0.3f);
        backSound.setLooping(true);
        backSound.start();


        matchSound = MediaPlayer.create(MainActivity.this,R.raw.matchknock);
        matchSound.setVolume(0.9f,0.9f);

        wrongSound = MediaPlayer.create(MainActivity.this,R.raw.wrong);
        wrongSound.setVolume(0.2f,0.2f);

        gameoverSound = MediaPlayer.create(MainActivity.this,R.raw.gameover);
        gameoverSound.setVolume(0.2f,0.2f);

        timerHandler = new Handler() {
            public void handleMessage(Message msg) {
                tvTime.setText("Time:"+"\n"+Integer.toString(time)); //this is the textview
            }
        };

        gameoverHandler = new Handler(){
            public void handleMessage(Message msg) {
                taskTimer.cancel();
                gameover();
            }
        };

        taskTimer.scheduleAtFixedRate(new LoopGame(),
                0, 1000);

        tvScore = findViewById(R.id.txScore);
        tvTime = findViewById(R.id.tvTime);
        for(int i=0; i < cells.length; i++) {

            buttonID = "cell" + (i + 1);

            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            cells[i] = ((Button) findViewById(resID));
        }

        board = new Board(cells,this);
        //i*mRow + j
        for(int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                final int i2 = i;
                final int j2 = j;
                cells[i*8+j].setOnClickListener(new MyOnClickListener(Score, time) {
                    public void onClick(View v) {

                        int num_block = board.blocksFinder(i2, j2, 0)+1;
                        System.out.println("num_block"+ (num_block));

                        if(num_block > 1) {

                            matchSound.start();
                            updateScore(num_block);
                            updateTime(num_block);
                            //System.out.println("Score: "+Score);
                            board.updateColors(v);

                            if(!board.isPlayable()){
                                System.out.println("Gameover is playable");
                                gameover();
                            }
                        }
                        else{
                            wrongSound.start();
                            board.resetBoardCheck();
                            //wrongSound.stop();
                        }

                    }
                });
            }
        }
    }
    @Override
    protected void onResume() {

        super.onResume();
        System.out.println("onResume");
        onPrepared(backSound);
        //time = 120;
        //Score = 0;
        taskTimer.scheduleAtFixedRate(new LoopGame(),
                0, 1000);


    }

    public void onPrepared(MediaPlayer player) {
        player.start();
    }
    @Override
    protected void onRestart(){
        super.onRestart();
        backSound.start();
    }
    @Override
    protected  void onPause(){
        super.onPause();

        System.out.println("onPause");
        backSound.stop();
    }
    @Override
    protected  void onStop(){
        super.onStop();
        backSound.stop();
    }
    public class MyOnClickListener implements View.OnClickListener
    {

        int score, time;
        View v;
        public MyOnClickListener(int score, int time) {
            this.score = score;
            this.time = time;

        }

        @Override
        public void onClick(View v)
        {
            //read your lovely variable
        }

    };

    public void updateScore (int num_block){

        /*
        * (x-1)*80+((x-2)/2)^2
        * */
        Score += ((num_block-1)*80+((num_block-2)/2)^2);
        tvScore.setText("Score:"+"\n"+Integer.toString(Score));

    }

    public class LoopGame extends TimerTask {

        @Override
        public void run() {
            if(time > 0) {
                time -= 1;
                //tvTime.setText(Integer.toString(time));
                timerHandler.obtainMessage(1).sendToTarget();
            }
            if(time <=0){
                gameoverHandler.obtainMessage(1).sendToTarget();

            }
        }
    }

    public void updateTime (int num_block){

        float t = 10+(((num_block-2)/3)^2)*20;
        //approximation by default
        time += Math.floor(t);;
    }

    public void gameover(){
        backSound.stop();
        gameoverSound.start();
        for(int i = 0; i < 64; i++){
            cells[i].setEnabled(false);
        }
        Intent intent = new Intent(this, GameOver.class);
        String outScore = Integer.toString(Score);
        System.out.println(Score);
        intent.putExtra("score", outScore);
        startActivity(intent);
    }
}
